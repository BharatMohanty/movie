package com.jsp.Recursion;
  class Power{
	  public static final boolean pow = false;

	public static int pow(int a,int b) {
		 if (a==1)return b;
		return b*pow(a-1, b);
	  }
  }
public class PowRecursion {
 public static void main(String[] args) {
	Power p1=new Power();
	Power p2=new Power();
	System.out.println(p1.pow);
	System.out.println(p2.pow);
}
}
