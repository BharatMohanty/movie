package com.jsp.Recursion;
  class Factorial{
	  public int factor(int num) {
		  if(num==0 || num==1) {
			  return 1;
		  }
		  else {
			  return num*factor(num-1);
		  }
	  }
  }
public class RecursionFactorial {
  public static void main(String[] args) {
	Factorial f=new Factorial();
	System.out.println(f.factor(6));
}
}
