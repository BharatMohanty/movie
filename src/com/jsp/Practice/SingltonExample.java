package com.jsp.Practice;
import java.util.Scanner;
class MovieHall{
	int seats = 100;
	private static MovieHall m=null;
	private MovieHall() {
	}
		public static MovieHall getMovieHall() {
			if (m==null)
				m=new MovieHall();
			return m;
		}
	public void reserveTicket(int n) {
		if(n>seats) {
			System.out.println(n+"seats not Available");
			return;
		}
		seats=seats - n;
		System.out.println(n+"seats are Reserved");
		System.out.println(seats+ "are Available");
	}
}
class BookingApp{
	 public static void booking() {
		 Scanner scn=new Scanner(System.in);
		 System.out.println("How many Tickets you Want:");
		 int n=scn.nextInt();
		 MovieHall m= MovieHall.getMovieHall();
		 m.reserveTicket(n);
  }
}
public class SingltonExample {
   public static void main(String[] args) {
	BookingApp.booking();
	BookingApp.booking();
  }
}

