package com.jsp.Practice;
class Test{
	private static Test t;
	private Test() {
//		System.out.println("this Value is:"+this.i);
	}
	public static Test getTest(){
		if(t==null) {
			t=new Test();
		}
		return t;
	}
}
public class SingletonClass {
  public static void main(String[] args) {
	Test t1= Test.getTest();
	Test t2=Test.getTest();
	System.out.println(t1==t2);
	System.out.println(t1.hashCode());
	
	
}
}
