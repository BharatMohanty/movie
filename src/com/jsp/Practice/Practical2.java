package com.jsp.Practice;
public class Practical2  
{     
     
    public static int A(int num){  
        int rem = 0, sum = 0;  
          
        
        while(num > 0){  
            rem = num%10;  
            sum = sum + (rem*rem);  
            num = num/10;  
        }  
        return sum;  
    }  
      
    public static void main(String[] args) {  
        int num = 82;  
        int result = num;  
          
        while(result != 1 && result != 4){  
            result = A(result);  
        }  
          
          
        if(result == 1)  
            System.out.println(num + " result");  
          
        else if(result == 4)  
            System.out.println(num + " result");     
    }  
}  