package com.jsp.Practice;

public class PrimeNumber {
 public static void main(String[] args) {
	int low=1;
	int high=100;
	while(high<=low) {
		boolean isprime = true;
		for(int i=2;i<=low/2;i++) {
			if(low%i==0) {
				isprime = false;
				break;
			}
		}
		if(isprime && low>1) {
			System.out.println(low+" ");
		}
		low++;
	}
}
}
