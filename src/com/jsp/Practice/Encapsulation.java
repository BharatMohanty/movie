package com.jsp.Practice;
import java.util.Scanner;
class Factorial{
	private int f;
	private int sum=0;
	public int fact (int n) {
		f=1;
		for(int i=1;i<=n;i++) {
			f=f*i;
		}
		return f;
	}
	public int getF() {
		return f;
	}
	public void setF(int f) {
		this.f=f;
	}
	public int getSum() {
		return sum;
	}
	public void setSum(int n) {
		while(n!=0) {
			int rem=n%10;
			sum=sum+fact(rem);
			n/=10;
		}
	}
}
public class Encapsulation {
    public static void main(String[] args) {
		Scanner scn= new Scanner(System.in);
		Factorial f=new Factorial();
		System.out.println("Enter any Number :");
		int n=scn.nextInt();
		f.setSum(n);
//		while(n!=0) {
		if(n==f.getSum())
			System.out.println("Strong");
		else 
			System.out.println("No");
	}
}

