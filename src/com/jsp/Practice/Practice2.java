package com.jsp.Practice;
class A5{
	int i;
	private A5(int i) {
		this.i=i;
	}
	public void display() {
		System.out.println("This value is:"+this.i);
	}
	public static A5 getInstance(int n) {
		return new A5(n);
	}
}
public class Practice2 {
  public static void main(String[] args) {
	A5 a1= A5.getInstance(45);
	A5 a2=A5.getInstance(67);
	System.out.println(a1==a2);
}
}
