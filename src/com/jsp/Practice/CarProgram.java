
package com.jsp.Practice;
interface Drivable{
	void start();
	void Accelerater ();
	void Horn();
	void stop();
	void Light();
	default public void Rooftop() {
		System.out.println("Rooftop Open and Close using the Special button");
	}
	static void ManualSystem() {
		System.out.println("All car have Manual System");
	}
}

class Bmw implements Drivable{
	public void start() {
		System.out.println("BMW car starts with Switch");
	}
	public void Accelerater() {
		System.out.println("Bmw Accelerater is too smooth");
	}
	public void Horn() {
		System.out.println("Bmw car Horn look like pneee pneee pneee.....");
	}
	public void Light() {
		System.out.println("Bmw light is very Powerfull");
	}
	public void stop() {
		System.out.println("Bmw car Stop");
	}
}
class Swift implements Drivable{
	public void start() {
		System.out.println("Swift car starts with Key");
	}
	public void Accelerater() {
		System.out.println("Swift Accelerater is  smooth");
	}
	public void Horn() {
		System.out.println("Swift car Horn look like pnuuu pnuuu pnuuuu.....");
	}
	public void Light() {
		System.out.println("Bmw light is not Powerfull");
	}
	public void stop() {
		System.out.println("Swift car Stop");
	}
	
	}

class Driver {
	Drivable d;
	Driver(Drivable d){
		this.d=d;
	}
	
	public void driver() {
		// TODO Auto-generated method stub
		Drivable.ManualSystem();
		d.start();
		d.Accelerater();
		d.Light();
		d.Horn();
		d.stop();
		d.Rooftop();
       if (d instanceof Bmw) {
    	   d.Rooftop();
       }
	}
  }

public class CarProgram {
  public static void main(String[] args) {
	Driver d1=new Driver(new Bmw());
	d1.driver();
	System.out.println("*******************");
	Driver d2=new Driver(new Swift());
	d2.driver();
  }	
}
  

