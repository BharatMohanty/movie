package com.jsp.WrapperClass;

public class WrapperExample1 {
  public static void main(String[] args) {
	int i=10;
//	System.out.println(i);
//	Integer i1=new Integer.valueOf(i); //Boxing 1.1 - 1.4
//	Integer i1=i;//AutoBoxing 1.5
//	System.out.println(i1);
//	int num = i1.intValue();//Unboxing
//	int num = i1;//Auto-Unboxing
//	System.out.println(num);
	Integer i1=new Integer(10); 
	Integer i2=new Integer(10);
	
	System.out.println(i1.hashCode());
	System.out.println(i2.hashCode());
	
	System.out.println(i1==i2);
	System.out.println(i1.equals(i2));
}
}
