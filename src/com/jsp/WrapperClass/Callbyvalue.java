package com.jsp.WrapperClass;
class MyWrapper{
	int n;
	public MyWrapper(int n) {this.n=n;}
	public String toString() {
		return n+"";
	}
}
public class Callbyvalue {
 public static void change(MyWrapper m) {
	 System.out.println("Inside Function: ");
	 m.n =m.n*7;
	 System.out.println("N value is inside function: "+ m.n);
	 System.out.println("**************************");
 }
 public static void main(String[] args) {
	int num=20;
	MyWrapper my=new MyWrapper(num);
	System.out.println(my.toString());
	change(my);
	System.out.println(my);
}
}
