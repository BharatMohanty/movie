package com.jsp.ConstructerCall;
class Demo1{
	
	public Demo1(int r) {
		System.out.println("Parameterised Demo 1 Constructor");
	}
}
class Demo2 extends Demo1{
	public Demo2(int r) {
	super(45);
		System.out.println("Parameterized Demo 2 Constructor");
	}
}
public class ParameterizedCall1 {
  public static void main(String[] args) {
	Demo2 d=new Demo2(78);
}
}
