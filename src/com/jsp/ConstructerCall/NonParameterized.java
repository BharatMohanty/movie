package com.jsp.ConstructerCall;
class Demo4{
	public Demo4() {
		System.out.println("No Parameterized Demo 4 Constructor");
	}
}	

class Demo3 extends Demo4{
	public Demo3() {
		System.out.println("No Parameterized Demo 3 Constructor");
	}
}
public class NonParameterized {
	 public static void main(String[] args) {
			Demo3 d=new Demo3();
		}
}

