package com.jsp.Methodoverriding;
class A{
	A(){
		System.out.println("A()");
	}
}
class B extends A{
	B(){
		System.out.println("B()");
	}
	B(int i){
		this();
		System.out.println("B(int)");
	}
}
public class Hidding {
  public static void main(String[] args) {
	System.out.println("Hello World");
}
}
