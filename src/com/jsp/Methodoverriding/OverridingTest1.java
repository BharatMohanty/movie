package com.jsp.Methodoverriding;
 class Father{
	 long mobile=7982142245l;
	 public void call() {
		 System.out.println(mobile);
	 }
 }
 class Daughter extends Father{
	 long mob=8280216163l;
	 public void call() {
		 System.out.println(mob);
	 }
	 public void eatable() {
		 System.out.println("I love Chocolate");
	 }
 }
public class OverridingTest1 {
  public static void main(String[] args) {
	  Father f=new Father();
	Daughter d=new Daughter();
	d.call();
	f.call();
	d.eatable();
}
}
