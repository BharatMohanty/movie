package com.jsp.Methodoverriding;
class Father1{
	String bike="TVS LUNA";
	public void buy() {
		System.out.println(bike);
	}
}
	class Son1 extends Father1{
		String bike="Dukati";
		public void buy() {
			System.out.println(bike);
		}
		
	}

public class OverridingTest2 {
  public static void main(String[] args) {
	Son1 s=new Son1();
	s.buy();
	Father1 f=new Father1();
	f.buy();
	Father1 f1=new Father1();
	f1.buy();
	
}
}

