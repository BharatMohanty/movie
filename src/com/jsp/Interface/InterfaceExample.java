package com.jsp.Interface;
interface  Animal{
	 void sound();
	 void eat(); 
	void movement();  
	 void drink() ;
}
class Lion implements Animal {

	@Override
	public void sound() {
		// TODO Auto-generated method stub
		System.out.println("Lion Roars");
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Lion eat Meat");
	}

	@Override
	public void movement() {
		// TODO Auto-generated method stub
		System.out.println("Lion runs Very Fast");
	}

	@Override
	public void drink() {
		// TODO Auto-generated method stub
		System.out.println("Lion Drinks Water");
	}

}
class InterfaceExample{
	public static void main(String[] args) {
		Animal a1=new Lion();
		a1.sound();
		a1.eat();
		a1.movement();
		a1.drink();
	}
}
