package com.jsp.MuiltiThread;
class Operation
{
public static void operate(int n)
{
	for(int i=1;i<=10;i++)
	{
		System.out.println((int)Math.pow(i, n))  ;
	}
}
}
class Square extends Thread
{
	Operation o = new Operation();
	public void run() {
		Operation.operate(2);
	}
}
class Cube extends Thread
{
	Operation o = new Operation();
	public void run() {
		Operation.operate(3);
	}
	
}
public class ExampleThread {
  public static void main(String[] args) {
	Square s = new Square();
	s.start();
	Cube c = new Cube();
	c.start();
	
}
}
