package com.jsp.Allmethod;

public class AllMethods {

	//Methods for Reverse.
	public static int reverse(int n)
	{
		int rev = 0;
		while(n>0)
		{
			int rem = n % 10;
			rev = rev * 10 + rem;
			n/=10;
		}
		return rev;
	}
	
	//Methods for finding factorial.
	public static long factorial(long n)
	{
		long fact = 1;
		for(int i=1;i<=n;i++)
		{
			fact = fact * i;
		}
		return fact;
	}
	
	//Method for finding length of number.
	public static int length(int n)
	{
		int len = 0;
		while(n>0)
		{
			len+=1;
			n/=10;
		}
		return len;
	}
	
	//Methods for x power y.
	public static long power(long x,long y)
	{
		long res = (long)Math.pow(x,y);
		return res;
	}
	
	//Methods for Strong number.
	public static long strong(int num)
	{	long sum = 0;
		while(num>0)
		{
			int rem = num % 10;
			sum = sum +  factorial(rem);
			num /=10;
		}
		return sum;
	}
	
	//Methods for Armstrong.
	public static long armstrong(int num)
	{
		int tmp = num;
		long sum = 0;
		while(num>0)
		{
			long rem = num % 10;
			sum = sum + power(rem,length(tmp));
			num /=10;
		}
		return sum;
	}
	
	//Methods for Niven(harshad) number.
	public static int nevin(int n)
	{
		int sum = 0;
		while(n>0)
		{
			int rem = n % 10;
			sum = sum + rem;
			n /= 10;
		}
		return sum;
	}
	
	//Methods for Automorphic number.
	public static int automorphic(int num)
	{
		int temp = num;
		int multi = (int)power(num, 2);
		int res = (int)(multi %  power(10, length(num)));
		return res;
	}
	
	//Methods for perfect number.
	public static int perfect(int n)
	{
		int sum = 0;
		for(int i=1;i<=n/2;i++)
		{
			if(n%i==0)
			{
				sum = sum + i;
			}
		}
		return sum;
	}
}


