package com.jsp.Methodprogram;

public class Noargumentsretun {
     public static int addition() {
    	 int a=78;
    	 int b=8;
    	 int res=a+b;
    	 return res;
//  another option of return type = return a+b
     }
     public static void main(String[] args) {
		System.out.println(addition());
//	another option of print = int result=addition();
	}
}
