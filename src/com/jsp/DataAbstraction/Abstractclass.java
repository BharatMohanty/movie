package com.jsp.DataAbstraction;
abstract class Animal{
	abstract public void sound();
	abstract public void eat(); 
	public void movement() {} 
	public void drink() {
		System.out.println("Water is to drink for Animal");
	}
}
abstract class Lion extends Animal{

	@Override
	public void sound() {
		// TODO Auto-generated method stub
		System.out.println("Roars");
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Flesh");
	}

}
class Babylion extends Lion{
	@Override
	public void movement() {
		// TODO Auto-generated method stub
		System.out.println("Lion is Running with legs");
	 }
	
	}
public class Abstractclass {
public static void main(String[] args) {
	Babylion b=new Babylion();
	b.sound();
	b.drink();
	b.eat();
	b.movement();
	
}
}
