package com.jsp.Constructer;
 class Cricket{
	 int i;
	 private Cricket(int i) {
		 this.i=i;
	 }
	 public static Cricket getInstance() {
		 return new Cricket(5);
	 }
	 public void display() {
		 System.out.println(i);
	 }
 }
public class PrivateConstructor {
  public static void main(String[] args) {
	Cricket c1=Cricket.getInstance();
	c1.display();
}
}
