package com.jsp.Constructer;
class Student1{
	int id;
	String name;
	String city;
	Student1(int i,String n){
		id=i;
		name=n;
	}
	Student1(int i,String n,String c){
		id=i;
		name=n;
		city=c;
	}
	public void display() {
		System.out.println(id+""+name+""+city);
	}
}
public class constructorOverloading {
  public static void main(String[] args) {
	Student1 s=new Student1(23,"Sipu");
	s.display();
	Student1 s1=new Student1(26,"Bharat","Odisha");
	s1.display();
}
}
