package com.jsp.Constructer;

class Cricket11{
	 int score;
	 String name;
	 private Cricket11(int s,String n) {
		 this.score=s;
		 this.name=n;
	 }
	 public static Cricket11 getInstance(int s,String n) {
		 return new Cricket11(s,n);
	 }
	 public void display(){
		 System.out.println(score+" "+name);
	 }
}
public  class PrivateConstuctortest1 {

	public static void main(String[] args) {
		
		Cricket11 c1=Cricket11.getInstance(88,"Bharat");
		Cricket11 c2=Cricket11.getInstance(44, "Sachin");
		Cricket11 c3=Cricket11.getInstance(84, "Satya");
		c1.display();
		c2.display();
		c3.display();
	}

}
