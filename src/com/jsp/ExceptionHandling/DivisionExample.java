package com.jsp.ExceptionHandling;
import java.util.Scanner;
public class DivisionExample {
  public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	double d = 0;
	int []arr = new int[4];
	System.out.println("Enter num1: ");
	int num1 = sc.nextInt();
	System.out.println("Enter num2: ");
	int num2 = sc.nextInt();
	try {
		d = num1/num2;
		System.out.println(d);
	}
	catch(ArithmeticException e) {
		System.out.println("Dont divide by Zero");
	}
	try {
		arr[4] = 10;
	}
	catch(ArrayIndexOutOfBoundsException e) {
		System.out.println("Inde is not Possible to Access");
	}
	System.out.println("Thank You");
	for(int i = 0;i<10;i++)System.out.println(i);
	
	}
}
