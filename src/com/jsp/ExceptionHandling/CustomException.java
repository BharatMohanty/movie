package com.jsp.ExceptionHandling;

import java.util.Scanner;

class CheckAge extends RuntimeException{
	String message;
	public CheckAge(String message)
	{
		this.message = message;
	}
	public String getmessage()
	{
		return message;
	}
}
public class CustomException {
   public static void main(String[] args) {
	Scanner scn = new Scanner(System.in);
	System.out.println("Enter the age: ");
	int age = scn.nextInt();
	try
	{
		if(age<18)
		{
			throw new CheckAge("Not Eligible to Vote");
		}
		else
		{
			System.out.println("Your are Eligible to Vote");
		}
	}
	catch(CheckAge e)
	{
		System.out.println(e.toString());
	}
	System.out.println("Thank You");
}
}
