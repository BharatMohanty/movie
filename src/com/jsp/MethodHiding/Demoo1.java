package com.jsp.MethodHiding;
class Person{
	String name;
	Person(String n){
		name=n;
	}
}
class Student extends Person{
	String sub;
	Student(String n,String s){
		super (n);
		sub=s;
	}
}
public class Demoo1 {
  public static void main(String[] args) {
	Student t=new Student("Bharat","How to eat Biriyani");
	System.out.println(t.name);
	System.out.println(t.sub);
}
}
