package com.jsp.ObjectClass;

public class VehicleClass {
   public static void main(String[] args) 
   {
	   
	// TODO Auto-generated method stub
	   EngineClass e1 =new EngineClass("Petrol",5);
	   EngineClass e2 =new EngineClass("Petrol",4);
	     
	   CarClass c1 = new CarClass("Swift",1890.87,e1);
	   CarClass c2 = new CarClass("Swift",1890.87,e2);
   
	   System.out.println(c1==c2);
	   System.out.println(c1.equals(c2));
   }
}
