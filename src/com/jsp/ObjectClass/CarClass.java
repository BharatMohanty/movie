package com.jsp.ObjectClass;

public class CarClass {
 String name;
 double price;
 EngineClass eng;
  public CarClass(String name, double price, EngineClass eng) {
	  super();
	  this.name = name;
	  this. price=price;
	  this.eng = eng;
	
 }
  public String toString() {
	  return "Carclass [name=" + name + ",price=" + price + ",eng=" + eng +"]";
  }
  public boolean equals(Object obj) {
	  if(!(obj instanceof CarClass))return false;
	  CarClass c= (CarClass)obj;
	  return this.name.equals(c.name)&& 
			  this.price == c.price &&
			  this.eng.equals(c.eng);
  }
}
