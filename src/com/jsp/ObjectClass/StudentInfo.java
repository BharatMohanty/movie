package com.jsp.ObjectClass;
class Student extends Object{
	int id;
	String name;
	public Student(int id,String name) {
		super();
		this.id=id;
		this.name=name;
	}
	public void display() {
		System.out.println("Student Id: " +id);
		System.out.println("Student Id: "+ name);
	}
	public String toString1() {
		return "Student[id=" + id +", name="+ name +"]";
	}
//	public String toString() {
//		return"Student ID:" + id + " \n "+"Student Name: " + name;
//	}
	public boolean equals(Object obj) {
		if(!(obj instanceof Student)) return false;
		Student s= (Student)obj;
		return this.id== s.id && this.equals(s.name);
	}
}
public class StudentInfo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  Student s1=new Student(1,"Sipiu");
  s1.display();
  System.out.println("################");
  Student s2=new Student(2,"Bharat");
  s2.display();
	}

}
