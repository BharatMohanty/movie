package com.jsp.ObjectClass;
class Circle{
	int radius;
	public Circle(int radius) {
		super();
		this.radius=radius;
	}
	public String toString() {
		return "Circle[radius=" + radius + "]";
	}
	public boolean equals(Object a) {
//		Circle c=(Circle)a;
//		return this.radius==c.radius;
		if (!(a instanceof Circle)) return false;
    	return this.radius==((Circle)a).radius; //Down casting
	}
	
}
class Rectangle{
	
}
public class CircleClass {
  public static void main(String[] args) {
	Circle c1=new Circle(12);
	Circle c2=new Circle(13);
	Rectangle r1=new Rectangle();
	System.out.println(c1);
	
	System.out.println("****************");
	System.out.println(c2);
	
	System.out.println("****************");
	System.out.println(c1.hashCode());
	
	System.out.println("*************");
	System.out.println(c2.hashCode());
	
	System.out.println("***********");
	System.out.println(c1==c2);
	
	System.out.println("********");
	System.out.println(c1.equals(r1));
}
}
