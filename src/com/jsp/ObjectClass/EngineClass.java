package com.jsp.ObjectClass;

public class EngineClass {
 String fueltype;
 int yod;
 public EngineClass(String fueltype,int yod) {
	 super();
	 this.fueltype = fueltype;
	 this.yod=yod;
 }
 public String toString() {
	 return "Engine [fueltype=" + fueltype + ", yod=" + yod +"]";
}
 public boolean equals(Object obj) {
	 if(!(obj instanceof EngineClass))return false;
	 EngineClass e =(EngineClass)obj;
	 return this.fueltype.equals(e.fueltype) && this.yod == e.yod;
 }
	 
 }

 
