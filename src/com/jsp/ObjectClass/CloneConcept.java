package com.jsp.ObjectClass;
class Address implements Cloneable{
	String Address;
	public Address(String Address) {
		super();
		this.Address = Address;
	}
	@Override
	public String toString() {
		return "Address [Address=" + Address + "]";
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		
		return super.clone();
	}
	
}
class Employee implements Cloneable{
	int id;
	String ename;
	String designation;
	Address addr;
	public Employee(int id,String ename,String designation,Address addr) {
		super();
		this.id = id;
		this.ename = ename;
		this.designation =designation;
		this.addr = addr;
		
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", ename=" + ename + ", designation=" + designation + ", addr=" + addr + "]";
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		
		Employee e= (Employee)super.clone();
		e.addr = (Address)addr.clone();
		return e;
	}		
}
public class CloneConcept {
   public static void main(String[] args)throws CloneNotSupportedException {
	Address add = new Address("Banglore");
	Employee e1 = new Employee(1,"Bharat","Devloper",add);
	System.out.println(e1);
	Employee e2 = (Employee)e1.clone();
	System.out.println(e2);
	System.out.println(e1 == e2);
	e2.addr.Address = "Manglore";
	System.out.println(e1);
	System.out.println(e2);
}
}
