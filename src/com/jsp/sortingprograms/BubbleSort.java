//sorting an array using bubble sort in ascending order is implemented here
//if you want to sort in descending order just put '<' in line number 11...........

package com.jsp.sortingprograms;

public class BubbleSort {
	
	public static void sort(int[] a) {
		for(int i=0;i<a.length-1;i++) {
			for(int j=0;j<a.length-1-i;j++) {
				if(a[j]<a[j+1]) {
					int temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
				}
			}
		}
	}

	public static void main(String[] args) {
		int[] a= {1,6,9,4,5,3,9,8};
		sort(a);
		for(int n:a) System.out.println(n);
	}
}
