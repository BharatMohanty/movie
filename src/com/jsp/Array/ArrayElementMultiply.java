package com.jsp.Array;

public class ArrayElementMultiply {

	public static void main(String[] args) {
		int [] a= {3,4,5,6,7,8,9};
		int mult=1;
		String s="";
		for(int i=0;i<a.length;i++) {
			mult*=a[i];
			s+=a[i];
			if(i!=a.length-1) s+="*";
		}
		s+="="+mult;
		System.out.println(s);
	}
}
