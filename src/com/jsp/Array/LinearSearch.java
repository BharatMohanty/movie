package com.jsp.Array;

public class LinearSearch {
 public static void main(String[] args) {
	int[]a= {4,6,1,2,5,8};
	System.out.println(search(a,5));
	System.out.println(search(a,7));
}
 static int search(int[]a,int s) {
	 for(int i=0;i<a.length;i++) {
		 if(a[i]==s)return i;
	 }
	return-1;  
 }
}
