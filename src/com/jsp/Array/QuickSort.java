package com.jsp.Array;

public class QuickSort {
 public static void main(String[] args) {
	int[]a={9,2,8,4,6,7,10,5};
	Sort(a,0,8);
	for(int n:a) {
		System.out.println(n);
	}
 }
 static void Sort(int []a,int start,int end) {
	 if(start>=end)return;
	 int pivot=a[(start + end)/2];
	 int i=start,j=end;
	 while(i<=j) {
		 while(a[i]<pivot)i++;
		 while(a[j]>pivot)j--;
		 if(i<=j) {
			 int temp=a[i];
			 a[i]=a[j];
			 a[j]=temp;
			 i++;
			 j--;
		 }
	  }
//	 Sort(a,start,j);
//	 Sort(a,i,end);
   }
}
