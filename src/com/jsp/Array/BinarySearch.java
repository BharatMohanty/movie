package com.jsp.Array;

public class BinarySearch {
 public static void main(String[] args) {
	int[]a= {3,4,6,8,9,10,23};
	System.out.println(search(a,9));
	System.out.println(search(a,7));
}
 static int search(int[]a,int s) {
	 int start=0,end=a.length-1;
	 while(start<=end) {
		 int mid=(start + end)/2;
		 if(s==a[mid])return mid;
		 else if(s<a[mid])end=mid-1;
		 else start=mid +1;
	 }
	 return-1;
 }
}
