package com.jsp.Array;

public class MergeProgram {
  public static void main(String[] args) {
	int []a= {4,8,9,1,2,5,6,7};
	Sort(a);
	for(int n:a) {
		System.out.println(n);
	}
}
  public static void Sort(int[]a) {
	  if(a.length==1)return;
	  int[]left=new int [a.length/2];
	  int[]right=new int[a.length-left.length];
	  for(int i=0;i<left.length;i++) {
		  left[i]=a[i];
	  }
	  int i=left.length;
	  for(int j=0;j<right.length;j++) {
		  right[j]=a[i];
		  i++;
	  }
	  Sort(left);
	  Sort(right);
	  merge(left,right,a);
  }
  private static void merge(int[]a,int[]b,int[]c) {
	  int i=0,j=0,k=0;
	  while(i<a.length && j<b.length) {
		  if(a[i]<b[j])c[k++]=a[i++];
		  else c[k++]=b[j++];
	  }
	  while(i<a.length)c[k++]=a[i++];
	  while(j<b.length)c[k++]=b[j++];
  }
}
