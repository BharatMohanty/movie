package com.jsp.Array;

public class SumofDaigonalElements {
public static void main(String[] args) {
	int[][]a= {
			{1,2,3,1},
			{4,5,6,1},
			{7,8,9,1},
			{1,1,1,1}
	};
	int sum=0;
	for(int i=0;i<a.length;i++) {
		sum+=a[i][i];
		if(a.length%2==1 && i==a.length/2)continue;
		sum+=a[i][a.length-1-i];
	}
	System.out.println(sum);
}
}
