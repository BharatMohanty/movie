package com.jsp.Array;

public class ArraySmallestElement{

	public static void main(String[] args) {
		int [] a= {3,4,5,6,2,8,9};
		int small=a[0];
		for(int i=1;i<a.length;i++) {
			if(a[i]<small) small=a[i];
		}
		System.out.println(small);
	}
}
