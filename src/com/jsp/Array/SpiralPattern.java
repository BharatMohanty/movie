package com.jsp.Array;

public class SpiralPattern {

	public static void main(String[] args) {
		
		int [][]a= new int[6][6];
		char m='r';
		int r=0;
		int c=-1;
		
		for(int i=1;i<=a.length*a.length;i++) {
			switch(m) {
			case 'r': a[r][++c]=i; 
					if(c==a.length-1 || a[r][c+1]!=0) m='d';
					break;
			case 'd': a[++r][c]=i; 
					if(r==a.length-1 || a[r+1][c]!=0) m='l';
					break;
			case 'l': a[r][--c]=i; 
					if(c==0 || a[r][c-1]!=0) m='u';
					break;
			case 'u': a[--r][c]=i; 
					if(a[r-1][c]!=0) m='r';
					break;
			}
		}
		display(a);
	}
	public static void display(int[][] a) {
		for(int[] temp:a) {
			for(int n:temp) {
				System.out.print(n+"\t");
			}
			System.out.println();
		}
	}
}
