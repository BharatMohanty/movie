package com.jsp.Array;

public class TwodimArray {
public static void main(String[] args) {
	int[][]a= {
			{1,2,3},
			{4,5,6},
			{7,8,9}
	};
	for(int i=0;i<a.length;i++) {
		for(int j=0;j<a[i].length;j++) {
			System.out.println(a[i][j]+"\t");
		}
		System.out.println();
	}
	System.out.println("Using for each Loop");
	for(int []temp:a) {
		for(int n:temp) {
			System.out.println(n+"\t");
		}
		System.out.println();
	}
}
}
