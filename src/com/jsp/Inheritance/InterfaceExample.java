package com.jsp.Inheritance;
interface A{
	public void m1();
}
interface B{
	public void m1();
}
class C implements A,B{
	public void m1() {
		System.out.println("m1 Method of class c Call");
	}
}
public class InterfaceExample {
    public static void main(String[] args) {
		C c1=new C();
		c1.m1();
	}
}
