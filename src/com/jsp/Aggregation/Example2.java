package com.jsp.Aggregation;
class Student{
	int id;
	String name;
	String Schoolname;
	Address address;
	public Student(int id,String name,String Schoolname,Address address) {
		this.id=id;
		this.name=name;
		this.Schoolname=Schoolname;
		this.address=address;
	}
	void display() {
		System.out.println(id+"\t"+name+"\t"+address.city+"\t"+address.state+"\t"+address.country);
	}

}
public class Example2 {
  public static void main(String[] args) {
	Address address1=new Address("Soro","Odisha","India");
	Address address2=new Address("Balasore","Odisha","India");
	Student s=new Student(2344,"Bharat","S.S.V.M",address1);
	Student s1=new Student(3456,"Tungurubhol","Govt.School",address2);
	s.display();
	s1.display();
			
  }

}
