package com.jsp.Aggregation;
class Employee{
	int id;
	String name;
	Address address;
	public Employee(int id,String name,Address address) {
		this.id=id;
		this.name=name;
		this.address=address;
	}
	void display() {
		System.out.println(id+"\t"+name+"\t"+address.city+"\t"+address.state+"\t"+address.country);
	}

}
public class Example1 {
  public static void main(String[] args) {
	Address address1=new Address("Soro","Odisha","India");
	Address address2=new Address("Balasore","Odisha","India");
	Employee e=new Employee(2344,"Bharat",address1);
	Employee e1=new Employee(3456,"Tungurubhol",address2);
	e.display();
	e1.display();
			
}
}
