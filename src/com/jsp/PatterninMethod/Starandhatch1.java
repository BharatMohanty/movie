package com.jsp.PatterninMethod;
import java.util.Scanner;
public class Starandhatch1 {
   public static void main(String[] args) {
	Scanner scn=new Scanner(System.in);
	System.out.println("Enter number of rows: ");
	int n=scn.nextInt();
	if(n==5) {
		for(int i=1;i<4;i++) {
			for(int j=1;j<=5;j++) {
				if(i<j) {
					System.out.print("#");
				}
				else {
					System.out.print("*");
				}
			}
			System.out.println();
		}
	}
	else {
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=n;j++) {
				if(i<j) {
					System.out.print("#");
				}
				else {
					System.out.print("*");
				}
			}
			System.out.println();
		}
	}
}
}	