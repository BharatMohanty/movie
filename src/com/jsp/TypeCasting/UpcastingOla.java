package com.jsp.TypeCasting;
class Ola{
	String name;
	String Brand;
	int seats;
	public Ola(String name,String Brand,int seats) {
		this.name=name;
		this.Brand=Brand;
		this.seats=seats;
	}

class CityTaxi extends Ola{
     String City;
	public CityTaxi(String name,String Brand,int seats,String City) {
	super(name,Brand,seats);
	this.City=City;
		 
	}
}
 static class Driver{
  public static void main(String[] args) {
	Ola a=new Ola("City","Honda",4);
	System.out.println(a.name);
	System.out.println(a.Brand);
	System.out.println(a.seats);
}
}
}
