package com.jsp.TypeCasting;
class Bank{
	int interest;
	public void interest() {
		System.out.println("8%" + interest);
		
	}
}
class SBI extends Bank{
	@Override
	public void interest() {
		interest=5;
		System.out.println("Sbi interest is:" + interest);
		
	}
}
class AXIS extends Bank{
	@Override
	public void interest() {
		interest=4;
		System.out.println("Axis interest is:" + interest);
		
	}
}
public class Bankers {
  public static void main(String[] args) {
	  	Bank obj=new SBI();
	  	obj.interest();
	  	Bank obj1=new AXIS();
	  	obj1.interest();
	  	
	  	
	
}
}
