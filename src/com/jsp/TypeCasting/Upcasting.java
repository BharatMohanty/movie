package com.jsp.TypeCasting;
class A{
	public static B a1;
	int a=45;
	int b=56;
	public void m1() {
		System.out.println(a +" " + b);
	}
}
class B extends A{
	int c=67;
	public void m1() {
		System.out.println("I am a Overriden method");
	}
	public void m2() {
		System.out.println(c);
	}
}
public class Upcasting {
   public static void main(String[] args) {
	A a1=new A();
	a1.m1();
	B b1=new B();
	b1.m1();
	A.a1=new B();//Up casting
	a1.m1();
	System.out.println(a1.a);
	System.out.println(a1.b);
}
}
