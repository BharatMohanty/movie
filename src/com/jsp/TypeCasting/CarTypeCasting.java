package com.jsp.TypeCasting;
class Car{
	public void start() {
		System.out.println("Start");
	}
	public void accelarter()
	{
		System.out.println("Accelerater");
	}
	public void stop()
	{
		System.out.println("Stop");
	}
}
class Maruti extends Car{

	public void start() // Override start method
	{
		System.out.println("Maruti car Start");
	}
	public void accelarter()
	{
		System.out.println("Maruti car Accelerater");
	}
	public void stop()
	{
		System.out.println("Maruti car Stop");
	}
	
	public void blutooth() {
		System.out.println("New features added as Blutooth");
	}
}
class Honda extends Car{
	public void start() // Override start method
	{
		System.out.println("Honda car Start");
	}
	public void accelarter()
	{
		System.out.println("Honda car Accelerater");
	}
	public void stop()
	{
		System.out.println("Honda car Stop");
	}
	public void sunroof() {
		System.out.println("New features added as Sun roof");
	}
	public void music() {
		System.out.println("New features added as Music System");
	}
	public void bluetooth() {
		System.out.println("New features added as Bluetooth");
		
	}
}
class Driver{
	public void driver(Car c) {
		c.start();
		c.accelarter();
		c.stop();
	}
	public void accessBluetooth(Car c) {
		if(c instanceof Honda) {
		Honda h=(Honda)c;
		h.bluetooth();
		}
	}
}
public class CarTypeCasting {
public static void main(String[] args) {
	
	Car c1=new Maruti();
	c1.start();
	c1.accelarter();
	c1.stop();
	Maruti s2=(Maruti)c1;
	s2.blutooth();
	
	System.out.println("***************");
	
	Car c2=new Honda();
	c2.start();
	c2.accelarter();
	c2.stop();
	Honda h2=(Honda)c2;
	h2.sunroof();
	h2.music();
	
	}
}
