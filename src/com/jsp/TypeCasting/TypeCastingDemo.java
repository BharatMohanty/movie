package com.jsp.TypeCasting;
class Animal{
	public void drink() {
		System.out.println("Drink Water");
	}
}
	class Lion extends Animal {
		public void sound() {
			System.out.println("lion do Roars");
		}
		public void family() {
			System.out.println("Lion is not a Domestic Animal");
		}
		
	}
	class Dog extends Animal {
		public void sound() {
			System.out.println("Dog do Bhaooo");
		}
		public void Family() {
			System.out.println("Dog is a Domestic Animal");
		}
		
	}
public class TypeCastingDemo {
 public static void main(String[] args) {
	Dog d1=new Dog();
	d1.drink();
	d1.sound();
	d1.Family();
	Animal a1=new Dog();
	Dog d2=(Dog)d1;
	System.out.println("**********");
	Lion l1=new Lion();
	l1.drink();
	l1.sound();
	l1.family();
	Animal a2=new Lion();
	Lion l2=(Lion)a2;
	
}
}
