package com.jsp.TypeCasting;
import java.util.Scanner;

class veg{
	public void Patato() {
		System.out.println("Patato is Available");
	}
	public void Carrot() {
		System.out.println("Carrot is Available");
	}
	public void Mushroom() {
		System.out.println("Mushroom is Available");
	}
	public void LadyFinger() {
		System.out.println("LadyFinger is Available");
	}
}
	class Patato extends veg{
		public void fries() {
			System.out.println("French firsh with more oils is not good for health ");
		}
	
}
	class Carrot extends veg{
		public void prepareHalwa() {
			System.out.println("Carrot Halwa is a traditional sweet,lets prepare ");
		}
	}
	class Mushroom extends veg{
		public void preparePokoda() {
			System.out.println("Mushroom Pokoda is the best for Snaks");
		}
	}
	class LadyFinger extends veg{
		public void preparecurry() {
			System.out.println("Lady Finger Curry is the best Curry");
		}
	}
	class Shop{
		public veg sell(String vn) {
			if(vn.equalsIgnoreCase("Potato"))return new Patato();
			if(vn.equalsIgnoreCase("Carrot"))return new Carrot();
			if(vn.equalsIgnoreCase("Mushroom"))return new Mushroom();
			if(vn.equalsIgnoreCase("LadyFinger"))return new LadyFinger();
			System.out.println(vn +" is not available");
			return null;
	}
}
public class Project {
 public static void main(String[] args) {
	Shop s=new Shop();
	Scanner scn=new Scanner(System.in);
	System.out.println("Vegitable? ");
	String vn = scn.next();
	veg v=s.sell(vn);
	if(v instanceof Patato)
		((Patato)v).fries();
	if(v instanceof Carrot)
		((Carrot)v).prepareHalwa();
	if(v instanceof Mushroom)
		((Mushroom)v).preparePokoda();
	if(v instanceof LadyFinger)
		((LadyFinger)v).preparecurry();
	
	
    }
  }


