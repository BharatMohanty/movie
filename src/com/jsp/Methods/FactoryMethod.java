package com.jsp.Methods;
class Factory{
	int j;
	int i;
	
	public Factory(int i,int j) {
		this.i=i;
		this.j=j;
	}
	public static Factory develop(int i, int j) {//Factory method
		return new Factory(i,j);
	}
	public void display() {
		System.out.println(i+" "+j);
	}
}
public class FactoryMethod {
     public static void main(String[] args) {
		Factory f1=Factory.develop(2,3);
		Factory f2=Factory.develop(3,4);
		Factory f3=Factory.develop(4,6);
		f1.display();
		f2.display();
		f3.display();
	}
}
