package com.jsp.Methods;

import java.util.Scanner;

public class NonstaticFactorial {
	
	public void  NonstaticFactorial (int num) {
		int fact=1;
		for (int i=1;i<=num;i++) {
			fact=fact*i;
		}
		System.out.println(fact);
	}
	
     public static void main(String[] args) {
	   
       Scanner scn=new Scanner(System.in);
    	System.out.println("Enter a Number :");
    	int n=scn.nextInt();
     	NonstaticFactorial m1 = new NonstaticFactorial();//Object Creation
    	m1.NonstaticFactorial(n);
	
    }
}
