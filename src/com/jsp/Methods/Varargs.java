package com.jsp.Methods;

public class Varargs {
   public static void grandTotal(int...products){
	   System.out.println("Length: " + products.length);
	for(int i=0;i<products.length;i++) {
		System.out.println(products[i]+" ");
	}
	System.out.println();
	long total=0;
	for(int item: products) {
		total= total + item;
	}
	System.out.println(total);
	double gst = total*0.18;
	double grandTotal=total + gst;
	System.out.println("SGST : "+ total * 0.09);
	System.out.println("CGST : "+ total * 0.09);
	System.out.println("18 % Tax : " +gst);
	System.out.println("Grand Total is: "+ grandTotal);
   }
	public static void main(String[] args)
	{
		grandTotal(5000,3000,2000);
		System.out.println(".........");
		grandTotal(4000);
	}
}

