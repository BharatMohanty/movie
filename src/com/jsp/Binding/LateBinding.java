package com.jsp.Binding;
class Parent{
	 public void bike() {
		System.out.println("TVs 50");
	}
}
class child extends Parent{
	
	 public void bike() {
		System.out.println("Tvs Apache RTR 180 Beast");
	}
}
public class LateBinding {
  public static void main(String[] args) {
	Parent p=new Parent();
	child c=new child();
	p.bike();
	System.out.println("***********");
	c.bike();
	System.out.println("***********");
	Parent p1=new child();
	p.bike();
}
}
