package com.jsp.Binding;
class A{
	private int i;
	public int getI(){//getter/accesser
		return i;
	}
	public void setI(int i) {//setter/mutator
		this.i=i;
	}
	
}
public class DataEncapsulation {
 public static void main(String[] args) {
	A obj = new A();
	obj.setI(100);
	System.out.println(obj.getI());
	
	
}
}
